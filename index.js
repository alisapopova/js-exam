class Tab {
  constructor(items) {
    this.tabItems = items;
  }

  onItemClick(event, items, page) {
    let target = event.target;
    if (target.classList.contains('tab__item') && Tab.prevItem !== target) {
      Tab.prevItem.classList.remove('_current');
      let pageChildren = Array.from(page.children);
      pageChildren.forEach(function (child) {
        if (child.getAttribute('data-type')) {
          page.removeChild(child);
        }
      });
      let dataContainer = document.createElement('div');
      dataContainer.setAttribute('data-type', 'item-data');
      dataContainer.innerHTML = target.itemData;
      page.appendChild(dataContainer);
      target.classList.add('_current');
    }
    Tab.prevItem = target;
  }

  onPageKeyDown(event, page) {
    if (event.which === 13 && document.activeElement.classList.contains('tab__item')) {
      Tab.prevItem.classList.remove('_current');
      document.activeElement.classList.add('_current');
        let pageChildren = Array.from(page.children);
        pageChildren.forEach(function (child) {
            if (child.getAttribute('data-type')) {
                page.removeChild(child);
            }
        });
        let dataContainer = document.createElement('div');
        dataContainer.setAttribute('data-type', 'item-data');
        dataContainer.innerHTML = document.activeElement.itemData;
        page.appendChild(dataContainer);
        Tab.prevItem = document.activeElement;
    }
  }

  render() {
    let itemsContainer = document.createElement('div');
    itemsContainer.classList.add('tab__items');

    let pageContainer = document.getElementsByClassName('page');
    let i = 0, itemsLength = this.tabItems.length;

    this.tabItems.forEach(function(item) {
      let tabItem = document.createElement('div');
      tabItem.classList.add('tab__item');
      tabItem.innerHTML = item.title;
      tabItem.itemData = item.data;
      if (i === itemsLength - 1) {
        tabItem.tabIndex = 0;
      }
      else {
        tabItem.tabIndex = i + 1;
      }
      if (i === 0) {
        tabItem.classList.add('_current');
        Tab.prevItem = tabItem;
      }

      itemsContainer.appendChild(tabItem);
      i++;
    });

    itemsContainer.addEventListener('click', (event) => this.onItemClick(event, itemsContainer, pageContainer[0]));
    document.addEventListener('keydown', (event) => this.onPageKeyDown(event, pageContainer[0]));

    return itemsContainer;
  }
}

// Входные данные
const tab = new Tab([
  {
    title: 'Общая информация',
    data: `Какая-то личная информация о пользователе`
  },
  {
    title: 'История',
    data: `Какая-то история действий пользователя`
  },
  {
    title: 'Друзья',
    data: `Какие-то люди`
  }
]);

// Вставляем Таб на страницу и вызываем метод рэендер
document.getElementById('tab').appendChild(tab.render());
